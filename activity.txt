1.List the books Authored by Marjorie Green.
	Author_tbl
	- author_id: 213-46-8915

	Author_Title_tbl
	- author_id: 213-46-8915
	- title_id: BU1032

	- author_id: 213-46-8915
	- title_id: BU2075
	
	Title_tbl
	- title_id: BU1032
	- tittle: The Busy Executive's Databsae Guide

	- title_id: BU2075
	- tittle: You Can Combat Computer Stress!
2.List the books Authored by Michael O'Leary.
	Author_tbl
	- author_id: 267-41-2394
	
	Author_Title_tbl
	- author_id: 267-41-2394
	- title_id: BU1111

	Title_tbl
	- title_id: BU1111
	- tittle: Cooking with Computers
3. Write the author/s of "The Busy Executives Database Guide".
	Author_tbl
	- author_id: 213-46-8915
	- author_lname: Green
	- author_fname: Marjorie
	- address: 309 63rd St. 411
	- city: Oakland
	- state: CA
	
	Author_title_tbl
	- author_id: 213-46-8915
	- title_id: BU1032

4. Identify the publisher of "But Is It User Friendly?".
	Author_Title_tbl
	- author_id: 238-95-7766
	- title_id: PC1035

	Publisher_tbl
	- pub_id: 1389
	- pub_name: Algodata Infosystems
	- city: Boston


5. List the books published by Algodata Infosystems.
	Title_tbl
	- title_id: BU1032
	- title: "The Busy Executives Database Guide

	- title_id: BU1111
	- title: Cooking with Computers

	- title_id: BU7832
	- title: Straight Talk About Computers

	- title_id: PC1035
	- title: But Is It User Friendly?

	- title_id: PC8888
	- title: Secrets of Silicon Valley

	- title_id: PC9999
	- title: Net Etiquette
	